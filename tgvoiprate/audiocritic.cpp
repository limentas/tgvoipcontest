#include "audiocritic.h"

#include "utilities.hpp"

#include <cmath>
#include <iostream>
#include <limits>
#include <cstdint>
#include <algorithm>
#include <cstdlib>

namespace tgvoiprate {

constexpr float AudioCritic::min_rate_result;
constexpr float AudioCritic::max_rate_result;
constexpr float AudioCritic::max_penalty;

void AudioCritic::removesEndSilense(std::vector<float> &audio)
{
    const float maxDelta = 2.0f / std::numeric_limits<int16_t>::max();
    size_t dataEndIndex = audio.size() - 1;
    for(; dataEndIndex > 0; --dataEndIndex)
    {
        if (std::abs(audio[dataEndIndex]) >= maxDelta)
            break;
    }

    if (dataEndIndex < audio.size() - 1)
        audio.erase(audio.begin() + static_cast<long>(dataEndIndex) + 1, audio.end());
}

float AudioCritic::rateCall(const std::vector<float> &saidAudio, const std::vector<float> &heardAudio)
{
    std::list<std::pair<unsigned long, unsigned long>> saidQuantums, heardQuantums;
    log("Analyzing said audio...");
    //Split sended audio into speech parts (quantums)
    findSpeechQuantums(saidAudio, saidQuantums);
    log("Analyzing heard audio...");
    //Split received audio into speech parts (quantums)
    findSpeechQuantums(heardAudio, heardQuantums);

    //Normalize speech quantums and find accordance between sended and received quantums
    std::list<QuantumStruct> normalizedQuantums;
    normalizeSpeechQuantums(saidQuantums, heardQuantums, normalizedQuantums);

    auto normalizationPenalty = rateUnnormalizedQuantums(saidQuantums, heardQuantums, normalizedQuantums);

    log("Comparing two audios...");
    auto snrPenalty = snrSeg(saidAudio, heardAudio, normalizedQuantums);

     auto evaluationValue = max_rate_result - normalizationPenalty - snrPenalty;
     return std::max(std::min(evaluationValue, max_rate_result), min_rate_result);
}

void AudioCritic::findSpeechQuantums(const std::vector<float> &audio, std::list<std::pair<unsigned long, unsigned long>> &quantums)
{
    const auto segmentSize = 5; //seconds
    const auto segmentSamplesCount = segmentSize * audio_sample_rate; //samples per segment

    float speechLowRange = 0.05f; //TODO:
    float fullAmplitude = 0;
    for(auto i = 0ul; i < audio.size(); ++i)
    {
        fullAmplitude += std::abs(audio[i]);
    }

    speechLowRange = fullAmplitude / audio.size();
    log("speech low range ", speechLowRange);

    auto firstSpeechSample = 0ul, lastSpeechSample = 0ul;
    bool quantumStarted = false;
    const auto minSilenseBetweenQuantums = audio_sample_rate * 0.3; //300ms
    const auto minimumQuantumSize = audio_sample_rate * 0.03; //30ms
    for(auto i = 0ul; i < audio.size(); ++i)
    {
        if (audio[i] >= speechLowRange) //sample is loud enough
        {
            if(!quantumStarted) //Conditions to start quantum
            {
                firstSpeechSample = lastSpeechSample = i;
                quantumStarted = true;
            }
            else //Conditions to continue quantum
            {
                lastSpeechSample = i;
            }
        }
        else //Sample is not loud enough
        {
            if (quantumStarted && i - lastSpeechSample > minSilenseBetweenQuantums)
            {
                if (lastSpeechSample - firstSpeechSample < minimumQuantumSize) //Quantum is too small
                {
                    quantumStarted = false;
                    continue;
                }
                log("Speech quantum starts at ", sampleToSeconds(firstSpeechSample), " ends at ", sampleToSeconds(lastSpeechSample));
                quantums.push_back(std::pair<int, int>(firstSpeechSample, lastSpeechSample));
                quantumStarted = false;
            }
        }
    }

    if (quantumStarted)
    {
        log("Speech quantum starts at ", sampleToSeconds(firstSpeechSample), " ends at ", sampleToSeconds(lastSpeechSample));
        quantums.push_back(std::pair<int, int>(firstSpeechSample, lastSpeechSample));
    }

    log("Total quantums: ", quantums.size());
}

void AudioCritic::normalizeSpeechQuantums(std::list<std::pair<unsigned long, unsigned long>> &saidQuantums,
                                           std::list<std::pair<unsigned long, unsigned long>> &heardQuantums,
                                           std::list<QuantumStruct> &normalizedQuantums)
{
    const int maxDelayMs = 1000; //ms
    const unsigned long maxDelaySamples = audio_sample_rate * maxDelayMs / 1000;

    const int maxLengthDifferenceMs = 500; //ms
    const long maxLengthDifferenceSamples = audio_sample_rate * maxLengthDifferenceMs / 1000;

    // Merge
    for(auto saidIt = saidQuantums.begin(); saidIt != saidQuantums.end(); )
    {
        bool added = false;
        for(auto heardIt = heardQuantums.begin(); heardIt != heardQuantums.end(); )
        {
            // heard quantum does not intersects the said one
            if (heardIt->first > saidIt->second + maxDelaySamples)
                break;

            if (saidIt->second <= heardIt->second + maxDelaySamples &&
                    std::abs(static_cast<long>(saidIt->second - saidIt->first) - static_cast<long>(heardIt->second - heardIt->first)) <= maxLengthDifferenceSamples)
            {
                auto meadDelay = (heardIt->second - saidIt->second + heardIt->first - saidIt->first) / 2;
                auto lengthRatio = 1.0f * (heardIt->second - heardIt->first) / (saidIt->second - saidIt->first);
                normalizedQuantums.push_back(QuantumStruct{saidIt->first, saidIt->second, heardIt->first, heardIt->second, meadDelay, lengthRatio});
                saidIt = saidQuantums.erase(saidIt);
                heardIt = heardQuantums.erase(heardIt);
                added = true;
                break;
            }

            //Trying to place several heard quantums in one said quantum
            for(auto heardItEnd = heardIt; heardItEnd != heardQuantums.end(); ++heardItEnd)
            {
                if (saidIt->second + maxDelaySamples < heardItEnd->second)
                {
                    break;
                }

                if (saidIt->second <= heardItEnd->second + maxDelaySamples &&
                        std::abs(static_cast<long>(saidIt->second - saidIt->first) - static_cast<long>(heardItEnd->second - heardIt->first)) <= maxLengthDifferenceSamples)
                {
                    auto meadDelay = (heardItEnd->second - saidIt->second + heardIt->first - saidIt->first) / 2;
                    auto lengthRatio = 1.0f * (heardItEnd->second - heardIt->first) / (saidIt->second - saidIt->first);
                    normalizedQuantums.push_back(QuantumStruct{saidIt->first, saidIt->second, heardIt->first, heardItEnd->second, meadDelay, lengthRatio});
                    saidIt = saidQuantums.erase(saidIt);
                    heardQuantums.erase(heardIt);
                    heardIt = heardQuantums.erase(heardItEnd);
                    added = true;
                    break;
                }
            }

            if (!added)
                ++heardIt;
            else
                break;
        }
        if (!added)
            ++saidIt;
    }
}

float AudioCritic::rateUnnormalizedQuantums(const std::list<std::pair<unsigned long, unsigned long> > &saidQuantums, const std::list<std::pair<unsigned long, unsigned long> > &heardQuantums,
                                            const std::list<AudioCritic::QuantumStruct> &normalizedQuantums)
{
    //No intersections in speech quantums
    if (normalizedQuantums.size() == 0)
        return max_penalty;

    auto totalLength = 0ul;
    log("normalized quantums: ", normalizedQuantums.size());
    for(auto normQ: normalizedQuantums)
    {
        log("[ ", sampleToSeconds(normQ.saidAudioBegin), ", ", sampleToSeconds(normQ.saidAudioEnd), " ] <-> [ ",
            sampleToSeconds(normQ.heardAudioBegin), ", ", sampleToSeconds(normQ.heardAudioEnd), " ]");
        totalLength += normQ.saidAudioEnd - normQ.saidAudioBegin;
        totalLength += normQ.heardAudioEnd - normQ.heardAudioBegin;
    }

    auto unnormalizedSaidLength = 0ul;
    for(auto saidQ: saidQuantums)
    {
        auto length = saidQ.second - saidQ.first;
        unnormalizedSaidLength += length;
        totalLength += length;
    }
    log("Total unnormalized said length: ", sampleToSeconds(unnormalizedSaidLength));

    auto unnormalizedHeardLength = 0ul;
    for(auto heardQ: heardQuantums)
    {
        auto length = heardQ.second - heardQ.first;
        unnormalizedSaidLength += length;
        totalLength += length;
    }
    log("Total unnormalized heard length: ", sampleToSeconds(unnormalizedHeardLength));
    log("Total length: ", sampleToSeconds(totalLength));
    auto penalty = 5.0f * (unnormalizedHeardLength + unnormalizedSaidLength) / totalLength;
    log("Unnormalized penalty: ", penalty);

    return penalty;
}

float AudioCritic::snrSeg(const std::vector<float> &saidAudio, const std::vector<float> &heardAudio,
                          const std::list<QuantumStruct> &normalizedQuantums)
{
    if (normalizedQuantums.empty())
        return 0.0f;

    const int fragmentSizeMs = 30; //ms
    const unsigned long fragmentSizeSamples = audio_sample_rate * fragmentSizeMs / 1000;
    auto totalQuantumsLength = 0ul;
    auto totalPenalty = 0.0f;

    for(const auto &quantum : normalizedQuantums)
    {
        auto quantumSnr = 0.0f;
        auto fragmentsCount = 0;
        for(auto fragmentStartSample = quantum.saidAudioBegin; fragmentStartSample <= quantum.saidAudioEnd && fragmentStartSample < saidAudio.size();
            fragmentStartSample += fragmentSizeSamples)
        {
            auto numerator = 0.0f;
            auto denominator = 0.0f;
            for(auto i = 0ul; i < fragmentSizeSamples; ++i)
            {
                numerator += std::pow(saidAudio[fragmentStartSample + i], 2.0f);
                denominator += std::pow(saidAudio[fragmentStartSample + i] - heardAudio[fragmentStartSample + quantum.meadDelay
                        + static_cast<size_t>(i * quantum.lengthRatio)], 2.0f);
            }

            if (denominator != 0)
            {
                auto fragmentSnr = std::log10(1.0f * numerator / denominator);
                if (fragmentSnr > -10 && fragmentSnr < 35)
                {
                    quantumSnr += fragmentSnr;
                    ++fragmentsCount;
                }
            }
        }

        totalQuantumsLength += quantum.heardAudioEnd - quantum.heardAudioBegin;

        if (fragmentsCount == 0) //Fragments are identical
            continue;

        quantumSnr = quantumSnr * 10 / fragmentsCount;

        log("Speech quantum [", sampleToSeconds(quantum.saidAudioBegin), ", ", sampleToSeconds(quantum.saidAudioEnd), "] "
                            "[", sampleToSeconds(quantum.heardAudioBegin), ", ", sampleToSeconds(quantum.heardAudioEnd), "] SNRseg: ", quantumSnr);

        if (std::abs(quantumSnr) >= 3.5f)
            totalPenalty += std::abs(quantumSnr * (quantum.heardAudioEnd - quantum.heardAudioBegin));
    }

    totalPenalty = totalPenalty / totalQuantumsLength;

    log("SNR penalty: ", totalPenalty);

    return totalPenalty;
}

float AudioCritic::sampleToSeconds(unsigned long sample)
{
    return 1.0f * sample / audio_sample_rate;
}

} //leave tgvoiprate namespace
