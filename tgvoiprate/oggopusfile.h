#ifndef OGGOPUSFILE_H
#define OGGOPUSFILE_H

#include <cstddef>
#include <cstdint>
#include <vector>

namespace tgvoiprate {

class OggOpusFile
{
public:
    OggOpusFile() = delete;
    ~OggOpusFile() = delete;

    static bool readAudio(const char &fileName, std::vector<float> &audio);
};

} //leave tgvoiprate namespace

#endif // OGGOPUSFILE_H
