#include "oggopusfile.h"
#include "audiocritic.h"
#include "utilities.hpp"

#include <iostream>
#include <memory>
#include <iomanip>

void usage()
{
    std::cout << "tgvoiprate /path/to/sound_A.opus /path/to/sound_output_A.opus" << std::endl;
}

using namespace tgvoiprate;

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        usage();
        return 1;
    }

    std::vector<float> saidAudio, heardAudio;

    log("Sounds to analize: ", argv[1], ", ", argv[2]);

    if (!OggOpusFile::readAudio(*(argv[1]), saidAudio))
    {
        std::cerr << "Error while processing the first file" << std::endl;
        return 2;
    }

    if (!OggOpusFile::readAudio(*(argv[2]), heardAudio))
    {
        std::cerr << "Error while processing the second file" << std::endl;
        return 3;
    }

    if (saidAudio.size() == 0 || heardAudio.size() == 0)
    {
        std::cout << 1.0 << std::endl;
        return 0;
    }

    //Removing silence from end. It can be there because of 3 seconds waiting ending of transmission in tgvoipcall
    AudioCritic::removesEndSilense(heardAudio);

    std::cout << std::setprecision(4) << std::fixed << AudioCritic::rateCall(saidAudio, heardAudio) << std::endl;

    return 0;
}
