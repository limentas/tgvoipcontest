TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(debug, debug|release) {
  DEFINES += _DEBUG
}

QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE *= -O3

INCLUDEPATH += /usr/include/opus

LIBS += -lopusfile

SOURCES += \
        audiocritic.cpp \
        main.cpp \
        oggopusfile.cpp

HEADERS += \
    audiocritic.h \
    oggopusfile.h \
    utilities.hpp
