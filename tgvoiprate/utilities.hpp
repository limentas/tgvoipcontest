#ifndef UTILITIES_H
#define UTILITIES_H

#include <iostream>

namespace tgvoiprate {

#ifdef _DEBUG
static inline void logPrivate() {}

template<typename First, typename ...Rest>
void logPrivate(First && first, Rest && ...rest)
{
    std::cout << std::forward<First>(first);
    logPrivate(std::forward<Rest>(rest)...);
}

template<typename First, typename ...Rest>
void log(First && first, Rest && ...rest)
{
    logPrivate(first, std::forward<Rest>(rest)...);
    std::cout << std::endl;
}
#else

template<typename ...Args>
void log(Args && ...)
{
}

#endif

} //leave tgvoiprate namespace

#endif // UTILITIES_H
