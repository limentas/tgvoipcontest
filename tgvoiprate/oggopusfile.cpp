#include "oggopusfile.h"

#include <opus/opusfile.h>  //needs to install debian package libopusfile-dev

namespace tgvoiprate {

bool OggOpusFile::readAudio(const char &fileName, std::vector<float> &audio)
{
    auto inputFile = ::op_open_file(&fileName, nullptr);
    if (!inputFile)
        return false;
    auto restPcmSize = static_cast<size_t>(::op_pcm_total(inputFile, -1));
    audio.resize(restPcmSize);
    auto dataPointer = audio.data();
    int samplesRead = 0;
    do
    {
        samplesRead = ::op_read_float(inputFile, dataPointer, static_cast<int>(restPcmSize), nullptr);
        if (samplesRead > 0)
        {
            restPcmSize -= static_cast<unsigned long>(samplesRead);
            dataPointer += samplesRead;
        }
    }
    while((samplesRead > 0) && (restPcmSize > 0));

    ::op_free(inputFile);
    return restPcmSize == 0;
}

} //leave tgvoiprate namespace
