#ifndef AUDIOCRITIC_H
#define AUDIOCRITIC_H

#include <cstdint>
#include <cstddef>
#include <vector>
#include <list>
#include <utility>

namespace tgvoiprate {

class AudioCritic
{
public:
    static void removesEndSilense(std::vector<float> &audio);
    static float rateCall(const std::vector<float> &saidAudio, const std::vector<float> &heardAudio);

private:
    struct QuantumStruct
    {
        unsigned long saidAudioBegin;
        unsigned long saidAudioEnd;

        unsigned long heardAudioBegin;
        unsigned long heardAudioEnd;

        unsigned long meadDelay;
        float   lengthRatio; //length ratio heard to said
    };

    static void findSpeechQuantums(const std::vector<float> &audio, std::list<std::pair<unsigned long, unsigned long>> &quantums);
    static void normalizeSpeechQuantums(std::list<std::pair<unsigned long, unsigned long>> &saidQuantums,
                                         std::list<std::pair<unsigned long, unsigned long>> &heardQuantums,
                                         std::list<QuantumStruct> &normalizedQuantums);
    static float rateUnnormalizedQuantums(const std::list<std::pair<unsigned long, unsigned long>> &saidQuantums,
                                          const std::list<std::pair<unsigned long, unsigned long>> &heardQuantums,
                                          const std::list<QuantumStruct> &normalizedQuantums);
    static float snrSeg(const std::vector<float> &saidAudio, const std::vector<float> &heardAudio, const std::list<QuantumStruct> &normalizedQuantums);

    static float sampleToSeconds(unsigned long sample);

    static const int audio_sample_rate = 48000; //Hz
    static constexpr float min_rate_result = 1.0f; //Minimum rate result, returned by application
    static constexpr float max_rate_result = 5.0f; //Maximum rate result, returned by application
    static constexpr float max_penalty = 4.0f; //Maximum penalty
};

} //leave tgvoiprate namespace

#endif // AUDIOCRITIC_H
