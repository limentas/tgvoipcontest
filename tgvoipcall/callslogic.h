#ifndef CALLSLOGIC_H
#define CALLSLOGIC_H

#include "oggopusfile.h"

#include <cstdio>
#include <string>
#include <memory>

namespace tgvoip {
    class VoIPController;
}

namespace tgvoipcall {

class OggOpusFile;

///Class incapsulates general logic
class CallsLogic
{
public:
    enum class RoleType
    {
        Caller,
        Callee
    };

    struct InputArguments
    {
        std::string     ipV4;
        uint16_t        port;
        unsigned char   peerTag[16];
        unsigned char   encryptionKey[256];
        std::string     inputSoundFile;
        std::string     outputSoundFile;
        std::string     configJson;
        RoleType        role;
    };

    CallsLogic() = default;
    ~CallsLogic();

    bool init(InputArguments &args);
    void deinit();

    bool isTransmissionStarted() const;
    bool isTransmissionFinished() const;

private:
    //reads audio data from file and fills buffer
    void pcmAudioInputCallback(int16_t *data, size_t size);

    //writes audio data from buffer to file
    void pcmAudioOutputCallback(int16_t *data, size_t size);

private:
    static const int64_t            endpoint_id = 1;
    tgvoip::VoIPController         *m_controller = nullptr;
    std::unique_ptr<OggOpusFile>    m_inputAudio;  ///< input audio file to send
    std::unique_ptr<OggOpusFile>    m_outputAudio; ///< output audio file received
    volatile bool                   m_transmissionStarted = false;
    volatile bool                   m_transmissionFinished = false;
};

} //leave tgvoipcall namespace

#endif // CALLSLOGIC_H
