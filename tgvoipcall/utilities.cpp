#include "utilities.h"

namespace tgvoipcall {

bool hexChar2Byte(char ch, unsigned char &res)
{
    int intRes;
    if ((ch >= '0') && (ch <= '9'))
        intRes = ch - '0';
    else if ((ch >= 'A') && (ch <= 'F'))
        intRes = 0x0A + (ch - 'A');
    else if ((ch >= 'a') && (ch <= 'f'))
        intRes = 0x0A + (ch - 'a');
    else
        return false;
    res = static_cast<unsigned char>(intRes);
    return true;
}

bool hex2ByteArray(const char *hexString, unsigned char *array, size_t bytesCount)
{
    for(auto i = 0u; i < bytesCount; ++i)
    {
        unsigned char low, high;
        if (!hexChar2Byte(hexString[2*i], high) || !hexChar2Byte(hexString[2*i + 1], low))
            return false;
        array[i] = high * 16 + low;
    }
    return true;
}

} //leave tgvoipcall namespace
