#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <iostream>

namespace tgvoipcall {

///< Transforms one hex character to it numeric value
bool hexChar2Byte(char ch, unsigned char &res);

///< Transfroms input string to byte array in direct order
bool hex2ByteArray(const char *hexString, unsigned char *array, size_t bytesCount);

#ifdef _DEBUG
static inline void logPrivate() {}

template<typename First, typename ...Rest>
void logPrivate(First && first, Rest && ...rest)
{
    std::cout << std::forward<First>(first);
    logPrivate(std::forward<Rest>(rest)...);
}

template<typename First, typename ...Rest>
void log(First && first, Rest && ...rest)
{
    logPrivate(first, std::forward<Rest>(rest)...);
    std::cout << std::endl;
}
#else

template<typename ...Args>
void log(Args && ...)
{
}

#endif

} //leave tgvoipcall namespace

#endif // UTILITIES_HPP
