#ifndef OGGOPUSFILE_H
#define OGGOPUSFILE_H

#include <ogg/ogg.h>        //needs to install debian package libogg-dev

#include <cstdio>
#include <fstream>
#include <memory>

class OggOpusFile;
class OpusEncoder;
struct OpusHead;

namespace tgvoipcall {

///< This class implements working with OPUS file format
class OggOpusFile
{
public:
    OggOpusFile() = default;
    ~OggOpusFile();

    bool openFileForRead(const char *fileName);
    bool openFileForWrite(const char *fileName);

    bool readSamples(void *data, size_t size);
    bool writeSamples(const void *data, size_t len, bool end);

private:
    void deinit();

    int prepareOpusHead(const OpusHead &head); ///< serialize OpusHead in m_internalBuffer
    int prepareOpusTags(const char *vendor, const char *comment1, const char *comment2); ///< serialize OpusTags in m_internalBuffer
    bool writeDataToOggStream(void *data, int encodedLength, int originalLength, bool end);

private:
    static const int opus_sample_rate = 48000; //Hz

    ::OggOpusFile   *m_inputFile = nullptr;
    ::OpusEncoder   *m_encoder = nullptr;
    std::unique_ptr<::ogg_stream_state>  m_outputStream;
    std::unique_ptr<std::ofstream>       m_outputFile;

    unsigned char m_internalBuffer[4096];

    bool        m_beginOggBitstream = true;
    int64_t     m_granulePos;
    int64_t     m_packetNo;
    int         m_preskip;
    int         m_encodedBytesInBuffer = 0;
    int         m_originalLengthInBuffer = 0;
};

} //leave tgvoipcall namespace

#endif // OGGOPUSFILE_H
