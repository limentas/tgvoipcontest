#include "utilities.h"
#include "callslogic.h"

#include <string.h>
#include <cstdio>

#include <iostream>
#include <string>

#include <thread>
#include <chrono>

using namespace tgvoipcall;

///< Application result codes
enum class ReturnCode
{
    Ok = 0,
    WrongCmdArgs = 1,       ///< Wrong command line arguments
    InitFailure = 2,        ///< Initialization failure
    ConnectionTimeout = 3   ///< Connecting establishing error
};

void usage()
{
    std::cout << "Usage: tgvoipcall reflector:port tag_caller_hex -k encryption_key_hex "
                 "-i /path/to/sound_A.opus -o /path/to/sound_output_B.opus -c config.json -r caller|callee" << std::endl;
}

ReturnCode parseCmdArgs(int argc, char *argv[], CallsLogic::InputArguments &args)
{
    const int ARGS_COUNT = 13;
    if (argc != ARGS_COUNT)
    {
        std::cerr << "Arguments count mismatch. Should be " << ARGS_COUNT << "." << std::endl;
        usage();
        return ReturnCode::WrongCmdArgs;
    }

    std::string addressStr(argv[1]);
    auto delimiterPos = addressStr.find(':');
    args.ipV4 = addressStr.substr(0, delimiterPos);
    auto portStr = addressStr.substr(delimiterPos + 1, addressStr.size() - delimiterPos - 1);

    try
    {
        args.port = static_cast<uint16_t>(stoi(portStr));
    }
    catch(const std::exception &)
    {
        std::cerr << "Can't parse port value" << std::endl;
        usage();
        return ReturnCode::WrongCmdArgs;
    }


    if (!hex2ByteArray(argv[2], args.peerTag, sizeof(args.peerTag)))
    {
        std::cerr << "Can't parse peer tag value" << std::endl;
        usage();
        return ReturnCode::WrongCmdArgs;
    }

    int i = 3;
    do
    {
        if(!strcmp(argv[i], "-k"))
        {
            if (!hex2ByteArray(argv[++i], args.encryptionKey, sizeof(args.encryptionKey)))
            {
                std::cerr << "Can't parse encryption key value" << std::endl;
                usage();
                return ReturnCode::WrongCmdArgs;
            }
        }
        else if(!strcmp(argv[i], "-i"))
        {
            args.inputSoundFile = argv[++i];
        }
        else if(!strcmp(argv[i], "-o"))
        {
            args.outputSoundFile = argv[++i];
        }
        else if(!strcmp(argv[i], "-c"))
        {
            args.configJson = argv[++i];
        }
        else if(!strcmp(argv[i], "-r"))
        {
            auto roleStr = argv[++i];
            if (!strcmp(roleStr, "caller"))
                args.role = CallsLogic::RoleType::Caller;
            else if (!strcmp(roleStr, "callee"))
                args.role = CallsLogic::RoleType::Callee;
            else
            {
                std::cerr << "Unknown role: \"" << roleStr << "\"" << std::endl;
                usage();
                return ReturnCode::WrongCmdArgs;
            }
        }
        else
        {
            std::cerr << "Unknown argument: \"" << argv[i] << "\"" << std::endl;
            usage();
            return ReturnCode::WrongCmdArgs;
        }
    }
    while(++i < argc);

    return ReturnCode::Ok;
}

int main(int argc, char *argv[])
{
    CallsLogic::InputArguments args;
    auto returnCode = parseCmdArgs(argc, argv, args);
    if (returnCode != ReturnCode::Ok)
        return static_cast<int>(returnCode);

    CallsLogic logic;

    if (!logic.init(args))
    {
        std::cerr << "Initialization failure" << std::endl;
        return static_cast<int>(ReturnCode::InitFailure);
    }

    using namespace std::chrono_literals;
    auto startTime = std::chrono::system_clock::now();
    const auto checkStep = 50ms;
    while(!logic.isTransmissionStarted() && (std::chrono::system_clock::now() - startTime < 5s))
    {
        std::this_thread::sleep_for(checkStep);
    }

    if (!logic.isTransmissionStarted())
    {
        std::cerr << "Connection timeout" << std::endl;
        return static_cast<int>(ReturnCode::ConnectionTimeout);
    }

    while(!logic.isTransmissionFinished())
    {
        std::this_thread::sleep_for(checkStep);
    }

    std::this_thread::sleep_for(3s);

    logic.deinit();
    return 0;
}
