#include "callslogic.h"
#include "oggopusfile.h"
#include "utilities.h"

#define TGVOIP_USE_CALLBACK_AUDIO_IO //libtgvoip must be compiled with the same flag
#include <VoIPController.h>
#include <VoIPServerConfig.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <functional>

#include <thread>
#include <chrono>

using namespace tgvoip;

namespace tgvoipcall {

void connectionStateChanged(tgvoip::VoIPController *, int state)
{
    log("New connection state: ", state);
}

void signalBarCountChanged(tgvoip::VoIPController *, int count)
{
    log("Signal bars: ", count);
}

void groupCallKeySent(tgvoip::VoIPController *)
{
    log("Group call key sent");
}

void groupCallKeyReceived(tgvoip::VoIPController *, const unsigned char *)
{
    log("Group call key received");
}

void upgradeToGroupCallRequested(tgvoip::VoIPController *)
{
    log("Upgrade to group call requested");
}

CallsLogic::~CallsLogic()
{
    deinit();
}

bool CallsLogic::init(CallsLogic::InputArguments &args)
{
    std::ifstream configStream;
    configStream.open(args.configJson);
    if(configStream.fail())
    {
        std::cerr << "Error while reading config file" << std::endl;
        return false;
    }
    std::stringstream confgFileContent;
    confgFileContent << configStream.rdbuf();
    ::ServerConfig::GetSharedInstance()->Update(confgFileContent.str());

    m_controller = new tgvoip::VoIPController();
    std::vector<Endpoint> endpoints({Endpoint(endpoint_id, args.port, IPv4Address(args.ipV4), IPv6Address(), Endpoint::Type::UDP_RELAY, args.peerTag)});
    m_controller->SetRemoteEndpoints(endpoints, false, tgvoip::VoIPController::GetConnectionMaxLayer());
    m_controller->SetEncryptionKey(reinterpret_cast<char *>(args.encryptionKey), args.role == RoleType::Caller);
    m_controller->SetNetworkType(NET_TYPE_WIFI);
    m_controller->SetAudioDataCallbacks(std::bind(&CallsLogic::pcmAudioInputCallback, this, std::placeholders::_1, std::placeholders::_2),
                                        std::bind(&CallsLogic::pcmAudioOutputCallback, this, std::placeholders::_1, std::placeholders::_2));
    m_controller->SetCallbacks(tgvoip::VoIPController::Callbacks({connectionStateChanged, signalBarCountChanged, groupCallKeySent, groupCallKeyReceived, upgradeToGroupCallRequested}));
    m_controller->Start();

    m_inputAudio.reset(new OggOpusFile());
    m_outputAudio.reset(new OggOpusFile());

    if (!m_inputAudio->openFileForRead(args.inputSoundFile.c_str()))
    {
        std::cerr << "Error while open input file" << std::endl;
        return false;
    }
    if (!m_outputAudio->openFileForWrite(args.outputSoundFile.c_str()))
    {
        std::cerr << "Error while open output file" << std::endl;
        return false;
    }

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(500ms); //waiting to run threads

    m_controller->Connect();

    return true;
}

void CallsLogic::deinit()
{
    m_inputAudio = nullptr;

    if (m_outputAudio)
    {
        m_outputAudio->writeSamples(nullptr, 0, true);
        m_outputAudio = nullptr;
    }

    if (m_controller)
    {
        m_controller->Stop();

        log("==================GetDebugString=====================");
        std::cout << m_controller->GetDebugString() << std::endl;

        delete m_controller;
        m_controller = nullptr;
    }
}

bool CallsLogic::isTransmissionStarted() const
{
    return m_transmissionStarted;
}

bool CallsLogic::isTransmissionFinished() const
{
    return m_transmissionFinished;
}

void CallsLogic::pcmAudioInputCallback(int16_t *data, size_t size)
{
    if (!m_inputAudio)
        return;

    m_transmissionStarted = true;

    if (!m_inputAudio->readSamples(data, size))
    {
        if (!m_transmissionFinished)
        {
            log("Input audio finished");
            m_transmissionFinished = true;
        }

        memset(data, 0, size * sizeof(int16_t));
    }
}

void CallsLogic::pcmAudioOutputCallback(int16_t *data, size_t size)
{
    if (!m_outputAudio)
        return;

    m_outputAudio->writeSamples(data, size, false);
}

} //leave tgvoipcall namespace
