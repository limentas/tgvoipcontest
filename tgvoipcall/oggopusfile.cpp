#include "oggopusfile.h"

#include <opus/opusfile.h>  //needs to install debian package libopusfile-dev
#include <opus/opus.h>      //needs to install debian package libopus-dev
#include <opus/opus_multistream.h>

#include <cstring>

namespace tgvoipcall {

OggOpusFile::~OggOpusFile()
{
    deinit();
}

bool OggOpusFile::openFileForRead(const char *fileName)
{
    deinit();
    return ((m_inputFile = op_open_file(fileName, nullptr)) != nullptr);
}

bool OggOpusFile::openFileForWrite(const char *fileName)
{
    deinit();

    m_outputFile.reset(new std::ofstream());
    m_outputFile->open(fileName, std::ios_base::out | std::ios_base::trunc);
    if (m_outputFile->fail())
        return false;

    m_outputStream.reset(new ogg_stream_state());
    if (ogg_stream_init(m_outputStream.get(), 1))
        return false;

    m_encoder = opus_encoder_create(opus_sample_rate, 1, OPUS_APPLICATION_AUDIO, nullptr);
    if (!m_encoder)
        return false;

    int lookahead;
    opus_encoder_ctl(m_encoder, OPUS_SET_BITRATE(64000));
    opus_encoder_ctl(m_encoder, OPUS_GET_LOOKAHEAD(&lookahead));

    m_preskip = lookahead;

    OpusHead head;
    head.version = 1;
    head.channel_count = 1;
    head.pre_skip = static_cast<unsigned int>(m_preskip);
    head.input_sample_rate = opus_sample_rate;
    head.output_gain = 0;
    head.mapping_family = 0;
    head.stream_count = 1;
    head.coupled_count = 0;
    auto headSize = prepareOpusHead(head);
    if (!writeDataToOggStream(m_internalBuffer, headSize, 0, false))
        return false;

    auto tagsSize = prepareOpusTags("tgvoipcall", "TITLE=Audio sample for Telegram VoIP Contest", "ORGANIZATION=slebe.dev");
    if (!writeDataToOggStream(m_internalBuffer, tagsSize, 0, false))
        return false;

    ogg_page page;
    while(ogg_stream_flush(m_outputStream.get(), &page))
    {
        m_outputFile->write(reinterpret_cast<const char*>(page.header), page.header_len);
        m_outputFile->write(reinterpret_cast<const char*>(page.body), page.body_len);
    }

    return true;
}

bool OggOpusFile::readSamples(void *data, size_t size)
{
    if (!m_inputFile)
        return false;
    auto samplesRead = op_read(m_inputFile, static_cast<opus_int16 *>(data), static_cast<int>(size), nullptr);
    return samplesRead > 0;
}

//For proper operation, you must call this function after the last samples with data = nullptr, len = 0 and end = true
bool OggOpusFile::writeSamples(const void *data, size_t len, bool end)
{
    if (!m_outputFile || !m_encoder || !m_outputStream)
        return false;

    //At first we collecting encoded data in buffer, and, when we receive next samples, we will put it to output stream
    //This is neccessary to last page is not be empty
    if (!end && m_encodedBytesInBuffer > 0)
    {
        if (!writeDataToOggStream(m_internalBuffer, m_encodedBytesInBuffer, m_originalLengthInBuffer, false))
            return false;
    }

    if (len > 0)
    {
        m_originalLengthInBuffer = static_cast<int>(len);
        m_encodedBytesInBuffer = opus_encode(m_encoder, static_cast<const opus_int16 *>(data), m_originalLengthInBuffer,
                                   m_internalBuffer, sizeof(m_internalBuffer));
    }

    if (end)
    {
        if (!writeDataToOggStream(m_internalBuffer, m_encodedBytesInBuffer, m_originalLengthInBuffer, true))
            return false;
    }

    ogg_page page;
    while(ogg_stream_pageout(m_outputStream.get(), &page) || (end && ogg_stream_flush(m_outputStream.get(), &page)))
    {
        m_outputFile->write(reinterpret_cast<const char*>(page.header), page.header_len);
        m_outputFile->write(reinterpret_cast<const char*>(page.body), page.body_len);
    }

    return true;
}

void OggOpusFile::deinit()
{
    if (m_inputFile)
    {
        op_free(m_inputFile);
        m_inputFile = nullptr;
    }

    if (m_encoder)
    {
        opus_encoder_destroy(m_encoder);
        m_encoder = nullptr;
    }

    if (m_outputStream)
    {
        ogg_stream_clear(m_outputStream.get());
        m_outputStream = nullptr;
    }

    if (m_outputFile)
    {
        m_outputFile->flush();
        m_outputFile->close();
        m_outputFile = nullptr;
    }
}

int OggOpusFile::prepareOpusHead(const OpusHead &head)
{
    auto bufferHead = m_internalBuffer;
    memcpy(bufferHead, "OpusHead", 8);
    bufferHead += 8;
    bufferHead[0] = static_cast<unsigned char>(head.version);
    bufferHead += 1;
    bufferHead[0] = static_cast<unsigned char>(head.channel_count);
    bufferHead += 1;
    auto preskip = static_cast<uint16_t>(head.pre_skip);
    memcpy(bufferHead, &preskip, 2);
    bufferHead += 2;
    memcpy(bufferHead, &head.input_sample_rate, 4);
    bufferHead += 4;
    auto gain = static_cast<uint16_t>(head.output_gain);
    memcpy(bufferHead, &gain, 2);
    bufferHead += 2;
    bufferHead[0] = static_cast<unsigned char>(head.mapping_family);
    bufferHead += 1;
    //Skip channel mapping table
    return static_cast<int>(bufferHead - m_internalBuffer);
}

int OggOpusFile::prepareOpusTags(const char *vendor, const char *comment1, const char *comment2)
{
    auto vendorLength = static_cast<unsigned int>(std::strlen(vendor));
    auto comment1LineLength = static_cast<unsigned int>(std::strlen(comment1));
    auto comment2LineLength = static_cast<unsigned int>(std::strlen(comment2));
    auto bufferHead = m_internalBuffer;
    unsigned int commentLinesCount = 2;
    memcpy(bufferHead, "OpusTags", 8);
    bufferHead += 8;
    memcpy(bufferHead, &vendorLength, 4);
    bufferHead += 4;
    memcpy(bufferHead, vendor, vendorLength);
    bufferHead += vendorLength;
    memcpy(bufferHead, &commentLinesCount, 4);
    bufferHead += 4;
    memcpy(bufferHead, &comment1LineLength, 4);
    bufferHead += 4;
    memcpy(bufferHead, comment1, comment1LineLength);
    bufferHead += comment1LineLength;
    memcpy(bufferHead, &comment2LineLength, 4);
    bufferHead += 4;
    memcpy(bufferHead, comment2, comment2LineLength);
    return static_cast<int>(bufferHead - m_internalBuffer + comment2LineLength);
}

bool OggOpusFile::writeDataToOggStream(void *data, int encodedLength, int originalLength, bool end)
{
    ogg_packet packet;
    packet.packet = reinterpret_cast<unsigned char *>(data);
    packet.bytes = encodedLength;
    packet.b_o_s = m_beginOggBitstream;
    m_beginOggBitstream = false;
    packet.e_o_s = end;
    m_granulePos += originalLength;
    packet.granulepos = m_granulePos;
    packet.packetno = m_packetNo++;

    return !ogg_stream_packetin(m_outputStream.get(), &packet);
}

} //leave tgvoipcall namespace
