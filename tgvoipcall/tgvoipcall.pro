TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

CONFIG(debug, debug|release) {
  DEFINES += _DEBUG
}

QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE *= -O3

INCLUDEPATH += /usr/local/include/tgvoip    \
    /usr/include/opus

LIBS += -L/usr/local/lib
LIBS += -ltgvoip -lopusfile -logg -lopus

SOURCES += \
    callslogic.cpp \
    main.cpp \
    oggopusfile.cpp \
    utilities.cpp

HEADERS += \
    callslogic.h \
    oggopusfile.h \
    utilities.h

